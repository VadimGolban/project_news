<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comments;
use AppBundle\Entity\Post;
use AppBundle\Form\CommentsType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Class NewsPublicController
 * @package AppBundle\Controller
 */
class NewsPublicController extends Controller
{
    /**
     * Class NewsPublicController
     * @return Response
     * @Route("/", name="homepage")
     * @Method({"GET", "POST"})
     */
    public function newsListAction()
    {
        $em = $this->getDoctrine()->getManager();
        $newsPosts = $em->getRepository('AppBundle:Post')->findAll();
        $newsCategories = $em->getRepository('AppBundle:Category')->findAll();

        return $this->render('@App/public/public_news_list.html.twig', [
            'news_posts' => $newsPosts,
            'news_categories' => $newsCategories,
        ]);
    }


    /**
     * Class NewsPublicController
     * @param Request $request
     * @param Post $newsId
     * @Route("/news/{news}/", name="single_news")
     * @Method({"GET", "POST"})
     *
     */
    public function showNewsAction(Request $request, EntityManagerInterface $em , Post $news)
    {
        $repository = $this->getDoctrine()->getManager();
        $singleNews = $repository->getRepository('AppBundle:Post')->find($news);

        if (!$singleNews) {
            throw  $this->createNotFoundException(
                'No News found in id' . $singleNews
            );
        }

        $comments= new Comments();
        $commentsForm = $this->createForm(CommentsType::class, $comments);
        $commentsForm->handleRequest($request);

        if ($commentsForm->isSubmitted() && $commentsForm->isValid()) {

            /** @var Comments $comments */
            $comments = $commentsForm->getData();
            $comments->setPost($news);
            $comments->setPublishedAt( new \DateTime('now'));

            $em->persist($comments);
            $em->flush();

            return $this->redirectToRoute('single_news', array(
                'news' => $news->getId()
            ));

        }
        return $this->render('@App/public/single_news.html.twig', array(
            'news_id' => $singleNews->getId(),
            'news_title' => $singleNews->getTitle(),
            'news_content' => $singleNews->getContent(),
            'news_author' => $singleNews->getAuthor(),
//              'news_published_at' =>$singleNews->getPublishedAt(),
            'news_images' => $singleNews->getFile(),
            'news_category' => $singleNews->getCategory(),
            'commentsForm' => $commentsForm->createView(),
            'news_comments' => $singleNews->getComments(),
        ));
    }

    public function saveCommentAction(Request $request)
    {
        $comments= new Comments();
        $commentsForm = $this->createForm(CommentsType::class, $comments);
        $commentsForm->handleRequest($request);

        if ($commentsForm->isSubmitted() && $commentsForm->isValid()) {

            /** @var Comments $comments */
            $comments = $commentsForm->getData();
            $comments->setPost($news);
            $comments->setPublishedAt( new \DateTime('now'));

            $em->persist($comments);
            $em->flush();

            return $this->redirectToRoute('single_news', array(
                'news' => $news->getId()
            ));

        }

        // redirect to news view page
    }
}