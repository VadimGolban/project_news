<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Comments;
use AppBundle\Entity\Post;
use AppBundle\Form\CommentsType;
use AppBundle\Form\NewsFilterType;
use AppBundle\Form\NewsType;
use AppBundle\Service\FileUploader;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class NewsController
 * @package AppBundle\Controller\Admin
 */
class NewsController extends Controller
{

    /**
     * Class NewsController
     * @Route("/admin/news/", name="admin_news")
     * @Method("GET")
     */
    public function indexAction() {
        return $this->render('@App/admin/news.html.twig');
    }

    /**
     * Class NewsController
     * @Route("/admin/news/add_news/", name="add_news")
     * @Method({"GET", "POST"})
     */
    public function createNewsAction(Request $request, EntityManagerInterface $em, FileUploader $fileUploader)
    {
        $news = new Post();
        $news->setPublishedAt( new \DateTime('tomorrow'));

        $form = $this->createForm(NewsType::class, $news);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $file = $news->getFile();
            $fileName = $fileUploader->upload($file);

            $news->setFile($fileName);
            $news = $form->getData();

            $em->persist($news);
            $em->flush();

            $this->addFlash(
                'notice',
                'News has been created!'
            );
            return $this->redirectToRoute('news_list');

        }

        return $this->render('@App/admin/add_news.html.twig' , array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/admin/news/news_list/", name="news_list")
     * @Method({"GET", "POST"})
     */
    public function NewsListAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $newsPosts = $em->getRepository('AppBundle:Post')->findAll();

        /** @var \Doctrine\ORM\QueryBuilder $qb */
        $qb = $this->getDoctrine()->getRepository(Post::class)->createQueryBuilder('n');

        $filterForm = $this->createForm(NewsFilterType::class);
        $filterForm->handleRequest($request);

        if ($filterForm->isSubmitted() && $filterForm->isValid()) {
                $qb
                 ->where('n.category = :category')
                 ->setParameter('category' , $filterForm->get('category')->getData());
        }

        $newsPosts = $qb->getQuery()->getResult();

        return $this->render('@App/admin/news_list.html.twig', [
            'news_posts' => $newsPosts,
            'filterForm' => $filterForm->createView(),
        ]);
    }
    /**
     * @param Request $request
     * @param Post $postNews
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/admin/news_edit/{id}/", name="news_edit")
     * @Method({"GET", "POST"})
     */
    public function updateNewsAction(Request $request, Post $postNews, FileUploader $fileUploader)
    {

//        if($request->)
        $postNews->setFile(
            new File($this->getParameter('files_directory').'/'.$postNews->getFile())
        );

        $form = $this->createForm(NewsType::class, $postNews );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $file = $postNews->getFile();
            $fileName = $fileUploader->upload($file);
            $postNews->setFile($fileName);

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $this->addFlash(
                'notice',
                'Your News has been updated!'
            );

            return $this->redirectToRoute('news_list');
        }

        return $this->render('@App/admin/news_edit.html.twig', [
            'form' => $form->createView()
        ]);

    }

    /**
     * @param Request  $request
     * @param Post $postNews
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/admin/delete_news/{id}/", name="delete_news")
     * @Method("GET")
     */
    public function deleteNewsAction(Request $request, Post $postNews)
    {
        if ($postNews === null) {
            return $this->redirectToRoute('news_list');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($postNews);
        $em->flush();

        return $this->redirectToRoute('news_list');
    }

    /**
     * Class NewsController
     * @param Request $request
     * @param Post $newsId
     * @Route("/admin/news/comments_list/{newsId}/", name="news_comments")
     * @Method({"GET", "POST"})
     *
     */
    public function CommentsListAction(Request $request, Post $newsId)
    {
        $em = $this->getDoctrine()->getManager();
        $newsComments = $em->getRepository('AppBundle:Post')->find($newsId);

        if (!$newsComments) {
            throw  $this->createNotFoundException(
                'No News Comments found in id' . $newsComments
            );
        }

        return $this->render('AppBundle:admin:comments_list.html.twig', array(
            'news_comments' => $newsComments->getComments(),
            'news_id' => $newsComments->getId(),
        ));
    }

    /**
     * @param Request $request
     * @param Comments $comments
     * @param Post $news
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @Route("/admin/news/update_comments/{comments}/{news}/", name="news_comments_edit")
     * @Method({"GET", "POST"})
     */
    public function updateNewsCommentsAction(Request $request, Comments $comments, Post $news)
    {
      $em = $this->getDoctrine()->getManager();
      $newsId = $em->getRepository('AppBundle:Post')->find($news);

      $commentsForm = $this->createForm(CommentsType::class, $comments);
      $commentsForm->handleRequest($request);
        if( $commentsForm->isSubmitted() && $commentsForm->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('news_comments', array(
                'newsId' => $newsId->getId(),
            ));
        }

        return $this->render('@App/admin/comments_edit.html.twig' , array(
            'commentsForm' => $commentsForm->createView(),
            'news' => $newsId->getId(),
        ));

    }

    /**
     * @param Request  $request
     * @param Post $news
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/admin/delete_comments/{id}/{news}/", name="delete_comments")
     * @Method("GET")
     */
    public function deleteNewsCommentsAction(Request $request, Comments $newsComments, Post $news)
    {
      if ($newsComments === null) {
          return $this->redirectToRoute('news_comments', array(
              'newsId' => $news->getId(),
          ));
      }

      $em = $this->getDoctrine()->getManager();
      $em->remove($newsComments);
      $em->flush();

        return $this->redirectToRoute('news_comments', array(
            'newsId' => $news->getId(),
        ));
    }

}