<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Category;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Form\CategoryType;

/**
 * Class CategoryController
 * @package AppBundle\Controller\Admin
 */
class CategoryController extends Controller
{

    /**
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     * Class CategoryController
     * @Route("/admin/news/add_category/", name="add_category")
     * @Method({"GET", "POST"})
     */
    public function createCategoryAction(Request $request , EntityManagerInterface $em)
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $category = $form->getData();

            $em->persist($category);
            $em->flush();
            $this->addFlash(
                'notice',
                'News Category has been created!'
            );
            return $this->redirectToRoute('category_list');
        }

        return $this->render('@App/admin/add_category.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @return Response
     * @Route("/admin/news/category_list/", name="category_list")
     * @Method("GET")
     */

    public function showCategoryAction()
    {
        $em = $this->getDoctrine()->getManager();
        $newsCategory = $em->getRepository('AppBundle:Category')->findAll();

        return $this->render('@App/admin/news_category_list.html.twig', [
            'news_category' => $newsCategory,
        ]);
    }

    /**
     * @param Request $request
     * @param Category $category
     * @return Response|RedirectResponse
     * @Route("/admin/category_edit/{id}", name="category_edit")
     * @Method({"GET", "POST"})
     */
    public function updateCategoryAction(Request $request, Category $category)
    {
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $this->addFlash(
                'notice',
                'News Category has been updated!'
            );
            return $this->redirectToRoute('category_list');

        }

        return $this->render('@App/admin/category_edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param Request  $request
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("admin/delete_category/{id}", name="delete_category")
//     * @Method("GET")
     */
    public function deleteCategoryAction(Request $request, Category $category)
    {
        if ($category === null) {
            return $this->redirectToRoute('category_list');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($category);
        $em->flush();

        return $this->redirectToRoute('category_list');
    }

}