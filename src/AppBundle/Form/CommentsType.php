<?php

namespace AppBundle\Form;

use AppBundle\Entity\Comments;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommentsType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user_name', TextType::class , array('label' => 'Name'))
            ->add('user_comments', TextareaType::class, array('label' => 'Your Comment'))
            ->add('user_email', TextType::class, array('label' => 'Email'))
//            ->add('published_at', DateType::class, array('widget' => 'choice', 'format' => 'dd-MM-yyyy', 'label' => 'Chose Date'))
            ->add('save', SubmitType::class, array('label' => 'Add comment'))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
//        $resolver->setDefaults(array(
//            'data_class' => Comments::class,
//        ));
    }
}

