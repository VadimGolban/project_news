<?php

namespace AppBundle\Form;

use AppBundle\Entity\Post;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewsType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array('label' => 'News title'))
            ->add('content', TextareaType::class, array('label' => 'Content'))
            ->add('author' , TextType::class, array('label' => 'Author Name'))
            ->add('published_at', DateType::class, array('widget' => 'choice', 'format' => 'dd-MM-yyyy', 'label' => 'Chose Date'))
            ->add('category' , EntityType::class, array(
                'class' => 'AppBundle:Category',
                'query_builder' => function (EntityRepository $er ) {
                    return $er->createQueryBuilder('c')
                    ->orderBy('c.name', 'DESC');
                },
                'choice_label' => 'name', 'label'=> 'News Category'))
            ->add('file', FileType::class, array('data_class'=> null, 'label' => 'File (img file)' ))
            ->add('save', SubmitType::class, array('label' => 'Add News'))
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Post::class,
        ));
    }
}