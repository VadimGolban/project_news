<?php

namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PublicMenuBuilder implements ContainerAwareInterface
{

    use ContainerAwareTrait;

//    private $container;
//
//    public function __construct(ContainerInterface $container)
//    {
//        $this->container = $container;
//    }

    public function navMainMenu(FactoryInterface $factory, array  $options)
    {

//        $em = $this->container->get('doctrine.orm.entity_manager');
//        $repository = $em->getRepository('AppBundle:Category');
//        $menuItems = $repository->findAll();

        $menu = $factory->createItem('root');

        $menu->setChildrenAttribute('class', ' nav navbar-nav ');
//
        $menu->addChild('News List', array('route' => 'homepage'));


        return $menu;

    }
}