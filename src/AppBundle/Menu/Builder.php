<?php

namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Builder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');

        $menu->setChildrenAttribute('class', ' nav navbar-nav ');

        $menu->addChild('News List' ,array('route' => 'news_list'));
        $menu->addChild('Add new News' ,array('route' => 'add_news'));
        $menu->addChild('Category List' ,array('route' => 'category_list'));
        $menu->addChild('Add new category' ,array('route' => 'add_category'));
        $menu->addChild('News List public' ,array('route' => 'homepage'));

        return $menu;
    }
}